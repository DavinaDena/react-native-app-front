import { Text, View } from "react-native";
import { Difficulty } from "../App/Entities";
import React from 'react';



interface Props{
    difficulty:Difficulty
}

export function DifficultyText({difficulty}: Props){


    return(
        <View>
            {/* <Text>{difficulty.id}</Text> */}
            <Text>{difficulty.question}</Text>
            {/* <LevelDifficultyMap key={difficulty.id}/> */}
        </View>
    )
}