import { useGetAllQAQuery, useGetByIdQAQuery } from "../App/getCoef-api"
import React from 'react';
import { Text, TouchableOpacity, View } from "react-native"
import { DifficultyText } from "./Difficulty";
import { LevelDifficultyText } from "./LevelDifficulty";
 
export interface NextQuestion{
    
}

export function Trytogetboth({route ,navigation}: any) {

    const { data, isLoading, isError } = useGetAllQAQuery()
    const { itemId, otherParam } = route.params.id ;
    const { } =useGetByIdQAQuery(Number(route.params.id))


    if (isLoading) {
        return <Text>Loading...</Text>
    }
    if (isError) {
        return <Text>Une erreur est survenue, veuillez recharger la page</Text>
    }

function whenPressed(){
    navigation.navigate('QA', { itemId: route.params.id[1]})
}


    return (
        <View>
            <View>

                {data?.map(item =>
                    <View key={item.id}> <DifficultyText difficulty={item} />
                        {item.levelDifficulty?.map(stuff =>
                            <View key={stuff.id}> <LevelDifficultyText levelDifficulty={stuff} /> </View>)}
                    </View>)}
            </View>

            <View>
                <TouchableOpacity >
                <Text>Next</Text>
                </TouchableOpacity>
            </View>




        </View>
    )
}