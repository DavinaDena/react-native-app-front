import { Button, Text, View } from "react-native";
import { LevelDifficulty } from "../App/Entities";
import React from 'react';


interface Props{
    levelDifficulty: LevelDifficulty
}


export function LevelDifficultyText({levelDifficulty}: Props){



    return(
        <View>
            {/* <Text> {levelDifficulty.id} </Text> */}
            <Text> {levelDifficulty.answers} </Text>
            {/* <Text> {levelDifficulty.coef} </Text> */}
        </View>
    )
}