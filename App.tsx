
import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from './App/store';
import {  LoginScreen } from './Screens/LoginScreen';
import { HomeScreen } from './Screens/HomeScreen';
import { RegisterScreen } from './Screens/RegisterScreen';
import { QuestionsScreen } from './Screens/QuestionsScreen';
import { UserScreen } from './Screens/UserScreen';
import { Trytogetboth } from './Components/tryToGetBothComponent';
import { TryButtons } from './Screens/TryButtons';

const Stack = createNativeStackNavigator()


export interface RouterParams {
  id: string
}
export interface NavProps{
  navigation: NativeStackNavigationProp <any>
}


export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="QA">
          <Stack.Screen name="Login"  component={LoginScreen}  />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          < Stack.Screen name="UserScreen" component={UserScreen} />
          < Stack.Screen name="QA" component={Trytogetboth} />
          <Stack.Screen name="TryButtons" component={TryButtons} />
        </Stack.Navigator>
      </NavigationContainer>
      </Provider>
  )
}

