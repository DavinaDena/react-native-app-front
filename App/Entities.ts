

export interface User{
    id?: number;
    name?: string;
    password?: string;
    email?: string;
    role?: string;
}

export interface Difficulty{
    id?: number;
    question?: string;
    levelDifficulty?: LevelDifficulty[];
}

export interface LevelDifficulty{
    id?: number;
    answers?: string;
    coef?: number;
    difficulty?: Difficulty;
}