import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { authApi } from './auth-api';
import authSlice from './auth-slice';
import { getCoefApi } from './getCoef-api';

export const store = configureStore({
    reducer: {
        [authApi.reducerPath]: authApi.reducer,
        [getCoefApi.reducerPath]: getCoefApi.reducer ,
        auth: authSlice,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(authApi.middleware, )
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;