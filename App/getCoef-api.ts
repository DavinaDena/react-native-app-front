import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import Constants from 'expo-constants';
import { Difficulty } from './Entities';




export const getCoefApi= createApi({
    reducerPath: 'getCoefApi',
    tagTypes: ['getCoef'],
    baseQuery: fetchBaseQuery({ baseUrl: Constants.manifest?.extra?.SERVER_URL + '/api/QA' }),
    endpoints: (builder) => ({

        getAllQA: builder.query<Difficulty[] , string|void>({
            query: ()=>({
                url: '/'
            })
        }),
        getByIdQA: builder.query<Difficulty, number>({

            query: (id)=>({
                url: '/'+ id
            })
        })
    })
})

export const {useGetAllQAQuery, useGetByIdQAQuery} = getCoefApi