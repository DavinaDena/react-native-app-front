import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import Constants from 'expo-constants';
import { AuthState } from './auth-slice';
import { User } from './Entities';



export const authApi = createApi({
    reducerPath: 'authApi',
    tagTypes: ['user'], 
    baseQuery: fetchBaseQuery({ baseUrl: Constants.manifest?.extra?.SERVER_URL+'/api/user'}),
    endpoints: (builder) => ({

        getThisUser: builder.query<User, void>({
            query: ()=> '/account',
            providesTags: ['user']
        }),

        getAllUsers: builder.query<User[], void>({
            query:()=>({
                url: '/',
                method: 'GET'
            })
        }),

        getUserById: builder.query<User, number>({
            query: (id)=> ({
                url: '/' +id
            })
        }),
        login: builder.mutation<AuthState, User>({
            query:(body)=>({
                url: '/login',
                method: 'POST',
                body
            })
        }),
        userRegister: builder.mutation<AuthState, FormData>({
            query: (body)=>({
                url:'/register',
                method: 'POST',
                body
            })
        })
    })
})

export const {useGetThisUserQuery, useGetAllUsersQuery, useGetUserByIdQuery,
useLoginMutation, useUserRegisterMutation} = authApi;