import React, {useEffect} from 'react';
import { Text, View } from 'react-native';
import { useLoginMutation } from '../App/auth-api';
import { Controller, useForm } from "react-hook-form";
import { Button, Input } from "react-native-elements";
import { NavProps } from '../App';
import * as SecureStore from 'expo-secure-store';
import { useAppSelector } from '../App/hooks';
import { User } from '../App/Entities';



export function LoginScreen({ navigation }: NavProps) {

    const user = useAppSelector(state => state.auth.user);
    const [login, { isLoading, error }] = useLoginMutation()
    const { control, handleSubmit, formState } = useForm()


    const onSubmit= async (data: User) =>{
        try {
            await login(data).unwrap();
            SecureStore.setItemAsync('credentials', JSON.stringify(data));
        } catch (error) {
            
        }
    }

    useEffect(() => {
        if (user) {
            navigation.navigate('UserScreen');
        }
        console.log(user);
        
    }, [user]);


    useEffect(() => {
        (async () => {
            const credentials = await SecureStore.getItemAsync('credentials');
            if (credentials) {
                login(JSON.parse(credentials));
            }
        })();
    }, []);

    const loginError = () => {
        if (error && 'status' in error) {
            if (error.status === 401) {
                return <Text>Credentials Error</Text>
            } else {
                return <Text>Login server error</Text>
            }
        }
    }
    return (
        <View>
        {loginError()}
        <Controller
            control={control}
            rules={{
                required: true,
            }}
            name="email"
            defaultValue=""
            render={({ field: { onChange, value } }) => (
                <Input
                    onChangeText={onChange}
                    value={value}
                    placeholder="Email"
                    errorMessage={formState.errors.email && "Email is required"}
                />)} />

        <Controller
            name="password"
            defaultValue=""
            control={control}
            rules={{
                required: true,
                maxLength: 100,
            }}
            render={({ field: { onChange, value } }) => (
                <Input
                    secureTextEntry={true}
                    onChangeText={onChange}
                    value={value}
                    placeholder="Password"
                    errorMessage={formState.errors.password && "Password is required"}
                />)} />

        <Button title="Submit" onPress={handleSubmit(onSubmit)} loading={isLoading} />

    </View>
        
    )
}

