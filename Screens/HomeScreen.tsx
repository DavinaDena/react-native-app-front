import React from 'react';
import { Button, Text, View } from 'react-native';
import { NavProps } from '../App';

export function HomeScreen({ navigation }: NavProps) {



    return (
        <View style={{ flex: 1, alignItems: 'center', marginTop: 30 }}>
            <View style={{ margin: 10 }}>
                <Button  title="Login" onPress={() => navigation.navigate("Login")}> Login</Button>

            </View>
            <View style={{ margin: 10 }}>
                <Button title="Register" onPress={() => navigation.navigate("Register")}> Register</Button>
            </View>

        </View>
    )
}
