import React from 'react';
import { Text, View } from 'react-native';
import { useAppSelector } from '../App/hooks';


export function UserScreen() {

    const user= useAppSelector(state => state.auth.user);



    return (
        <View>
            <Text>Hello {user?.name}</Text>
            <Text>{user?.id}</Text>
            <Text>{user?.name}</Text>
            <Text>{user?.email}</Text>
        </View>
    )
}
