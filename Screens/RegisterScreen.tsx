
import React, { useEffect } from 'react';
import { Text, View } from "react-native";
import { useForm, Controller } from "react-hook-form";
import {  useUserRegisterMutation } from '../App/auth-api';
import { Input, Button } from "react-native-elements";
import { NavProps } from '../App';
import * as SecureStore from 'expo-secure-store';
import { useAppDispatch, useAppSelector } from '../App/hooks';
import { User } from '../App/Entities';
import { setCredentials } from '../App/auth-slice';



export function RegisterScreen({ navigation }: NavProps) {

    const user = useAppSelector(state => state.auth.user);
    const [postRegister, { isLoading, error }] = useUserRegisterMutation()
    const { control, handleSubmit, formState } = useForm<User>();
    let dispacth = useAppDispatch()

    const onSubmit = async (data: any) => {
        try {
            let newUser = await postRegister(data).unwrap()
            SecureStore.setItemAsync('credentials', JSON.stringify(newUser));
            dispacth(setCredentials(newUser))
        } catch (error) {

        }
    }

    useEffect(() => {
        if (user) {
            navigation.navigate('UserScreen');
        }
        console.log(user);
    }, [user]);


    const registerError = () => {
        if (error && 'status' in error) {
            if (error.status === 401) {
                return <Text>Credentials Error</Text>
            } else {
                return <Text>Login server error</Text>
            }
        }
    }


    return (
        <View>
            {registerError()}


            <Controller
                control={control}
                rules={{
                    required: true,
                }}
                render={({ field: { onChange, value } }) => (
                    <Input
                        onChangeText={onChange}
                        value={value}
                        placeholder="name"
                        errorMessage={formState.errors.name && "Nom is required"}
                    />
                )}
                name="name"
                defaultValue=""
            />


            <Controller
                control={control}
                rules={{
                    required: true,
                    maxLength: 100,
                }}
                render={({ field: { onChange, value } }) => (
                    <Input
                        secureTextEntry={true}
                        onChangeText={onChange}
                        value={value}
                        placeholder="password"
                        errorMessage={formState.errors.password && "Password is required"}
                    />
                )}
                name="password"
                defaultValue=""
            />


            <Controller
                control={control}
                rules={{
                    required: true,
                    maxLength: 100,
                }}
                render={({ field: { onChange, value } }) => (
                    <Input
                        onChangeText={onChange}
                        value={value}
                        placeholder="Email"
                        errorMessage={formState.errors.email && "Email is required"}
                    />
                )}
                name="email"
                defaultValue=""
            />

            <Button title="Submit" onPress={handleSubmit(onSubmit)} loading={isLoading} />
        </View>
    )
}